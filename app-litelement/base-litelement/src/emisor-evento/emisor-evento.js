import { LitElement, html } from 'lit-element';

class EmisorEvento extends LitElement {

    static get properties() {
        return {

        };
    }

    constructor() {
        super()

    }

    sendEvent(e) {
        console.log("sendEvent");
        console.log(e);

        this.dispatchEvent(new CustomEvent("test-event", {
            detail: {
                course: "Techu",
                year: 21
            }
        }));
    }

    render() {
        return html`
            <div>Emisor evento</div>
            <button @click="${this.sendEvent}">No pulsar</button>
        `;
    }

}

customElements.define("emisor-evento", EmisorEvento);
//("etiqueta", clase a la que se refiere)