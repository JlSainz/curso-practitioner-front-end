import { LitElement, html } from 'lit-element';

class PersonaMainDm extends LitElement {

    static get properties() {
        return {
            people: Array
        };
    }

    constructor() {
        super();
        this.people = [
            {
                name: "Toreto",
                yearsInCompany: 10,
                profile: "A todo gas",
                photo: {
                    "src": "./img/persona.jpg",
                    "alt": "Toreto"
                },

            }, {
                name: "Bryan",
                yearsInCompany: 2,
                profile: "A todo gas 2",
                photo: {
                    "src": "./img/persona.jpg",
                    "alt": "Bryan"
                },

            }, {
                name: "Benito",
                yearsInCompany: 5,
                profile: "Lo que el viento se llevo",
                photo: {
                    "src": "./img/persona.jpg",
                    "alt": "Benito"
                },

            }, {
                name: "Ridick",
                yearsInCompany: 9,
                profile: "Cronicas",
                photo: {
                    "src": "./img/persona.jpg",
                    "alt": "Ridick"
                },

            }, {
                name: "Harry",
                yearsInCompany: 1,
                profile: "Potter",
                photo: {
                    "src": "./img/persona.jpg",
                    "alt": "Harry"
                },

            }
        ];

    }

    updated(changedProperties) {
        console.log("updated")
        if (changedProperties.has("people")) {
            console.log("Cmbiado propieda persona-main-dm")
            this.dispatchEvent(new CustomEvent("people-data-updated", {
                detail: {
                    people: this.people
                }
            }
            ))
        }
    }



    render() {
        return html`
        `;
    }

}

customElements.define("persona-main-dm", PersonaMainDm);
