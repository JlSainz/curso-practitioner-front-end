import { LitElement, html } from 'lit-element';

class FichaPersona extends LitElement {

    static get properties(){
        return {
            name: String,
            yearsInCompany: Number,
            personInfo: String
        };
    }

    constructor(){
        super()
        this.name = "Marcos";
        this.yearsInCompany = "5";
    }


    updated(changedProperties){
        console.log("updated");

        changedProperties.forEach((oldValue, propName) => {
            console.log("Propiedad " + propName + " cambia valor, anterior era " + oldValue);
        })

        if(changedProperties.has("name")){
            console.log("Propiedad name ha cambiado de valor, anterior era " + changedProperties.get("name") + " nuevo es " + this.name);
        }

        if(changedProperties.has("yearsInCompany")){
            console.log("Propiedad yearsInCompany ha cambiado de valor, anterior era " + changedProperties.get("yearsInCompany") + " nuevo es " + this.yearsInCompany);
            this.updatePersonInfo();
        }
    }

    updatePersonInfo(){
        if(this.yearsInCompany >= 7){
            this.personInfo = "lead";
        }else if(this.yearsInCompany >= 5){
            this.personInfo = "senior";
        }else if(this.yearsInCompany >= 3){
            this.personInfo = "junior";
        }   
    }

    render() {
        return html `
            <div>
                <label>Nombre completo</label>
                <input 
                    type="text" 
                    id="fname" 
                    name="fname"
                    value="${this.name}"
                    @input="${this.updateName}">
                <br>
                <label>Años en la empresa</label>
                <input 
                    type="text" 
                    name="yearsInCompany"
                    value="${this.yearsInCompany}"
                    @input="${this.updateYearsInCompany}">
                <input 
                    type="text" 
                    name="yearsInCompany"
                    value="${this.personInfo}"
                    @input="${this.updateYearsInCompany}">
            </div>
        `;
    }

    updateName(e){
        console.log("updateName");
        this.name = e.target.value;
    }

    updateYearsInCompany(e){
        console.log("updateYearsInCompany");
        this.yearsInCompany = e.target.value;
    }
}

customElements.define("ficha-persona", FichaPersona);
//("etiqueta", clase a la que se refiere)