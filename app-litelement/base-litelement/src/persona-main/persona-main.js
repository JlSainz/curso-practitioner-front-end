import { LitElement, html } from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';
import '../persona-main-dm/persona-main-dm.js';

class PersonaMain extends LitElement {

    static get properties() {
        return {
            people: Array,
            showPersonForm: Boolean,
            maxYearsInCompanyFilter: Number

        };
    }

    constructor() {
        super()
        this.people = [];
        this.showPersonForm = false;

    }

    updated(changedProperties) {
        console.log("updated");
        console.log(this.showPersonForm);
        if (changedProperties.has("showPersonForm")) {
            console.log("Ha cambiado el valor de la propiedad showPersonForm ---> persona-main");
            if (this.showPersonForm === true) {
                this.showPersonFormData();
            } else {
                this.showPersonList();
            }
        }

        if (changedProperties.has("people")) {
            console.log("Ha cambiado el valorde la propiedad showPersonForm en persona main");

            this.dispatchEvent(new CustomEvent('updated-people', {
                detail: {
                    people: this.people
                }
            }))
        }

        if(changedProperties.has("maxYearsInCompanyFilter")){
            console.log("Ha cambiado el valor de maxYearsInCompany")
            console.log(this.maxYearsInCompanyFilter)
        }
    }

    showPersonList() {
        console.log("showPersonList");
        console.log("Mostrando el listado de personas");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
    }

    showPersonFormData() {
        console.log("showPersonFormData");
        console.log("Mostrando el formulario de personas");
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
    }

    personaFormClose() {
        console.log("persona-form-close ----> persona-main");
        this.showPersonForm = false;
    }

    personaFormStore(e) {
        console.log("personFormStore");
        console.log("Se va a almacenar una persona");
        console.log(e.detail.person);

        if (e.detail.editingPerson === true) {
            console.log("Se actualiza la persona de nombre " + e.detail.person.name);
            this.people = this.people.map(
                person => person.name === e.detail.person.name
                    ? person = e.detail.person : person);

        } else {
            console.log("Se va a guardar una persona");
            this.people = [...this.people, e.detail.person];
        }

        this.showPersonForm = false;
    }

    deletePerson(e) {
        console.log("deletePerson en persona-main")
        this.people = this.people.filter(
            person => person.name != e.detail.name
        );
    }

    infoPerson(e) {

        console.log("infoPerson " + e.detail.name);
        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );

        let person = {};
        person.name = chosenPerson[0].name;
        person.profile = chosenPerson[0].profile;
        person.yearsInCompany = chosenPerson[0].yearsInCompany;

        this.shadowRoot.getElementById("personForm").person = person;
        this.shadowRoot.getElementById("personForm").editingPerson = true;
        this.showPersonForm = true;

    }

    peopleDataUpdated(e){
        console.log("peopleDataUpdated");
        this.people = e.detail.people;
    }


    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
                integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <div class="row cols-1 row-cols-sm-4">
                    ${this.people.filter(
                        person => person.yearsInCompany <= this.maxYearsInCompanyFilter
                    ).map(
                    person => html`
                    <persona-ficha-listado fname="${person.name}" yearsInCompany="${person.yearsInCompany}"
                        profile="${person.profile}" .photo="${person.photo}" @delete-person="${this.deletePerson}"
                        @info-person="${this.infoPerson}">
                    </persona-ficha-listado>
                    `
                )}
                </div>
            </div>
            <div class="row">
                <persona-form id="personForm" class="d-none border rounded border-primary"
                    @persona-form-close="${this.personaFormClose}" @persona-form-store="${this.personaFormStore}"></persona-form>
            </div>
           <persona-main-dm @people-data-updated="${this.peopleDataUpdated}"></persona-main-dm>
        `;
    }

}

customElements.define("persona-main", PersonaMain);
